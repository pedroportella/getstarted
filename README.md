# Getstarted

This is a demo web-application for the purpose of Luminary's Front End Developer Technical Test job application.

I've focused the first 4 hours of development to showcase my ability to architect and organise a front-end application for the purpose of Maintainability, Reusability, Scalability and Performance.

This demo app uses Angular 5 + Bootstrap 4 and all assets were exported from Sketch - provided by Luminary - as SVG (with one exception for the meeting room photo).

Due to the time constraint, I've prioritised the work on header, footer, and the first two elements of the home page (jumbotron and welcome-text). Mobile first up to 992px wide.

If I had more time, I would have implemented the look-and-feel for large desktops as well as finalising the other elements of the home page (services section and featured blog articles section).

Finally, everything you see here you can, actually, see on your mobile and computer here: https://getstarted.luminary.pedroportella.com
This shows my crucial ability to deploy code to Production. #DevOpsToSuccess

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.

## Install dependencies

Run `npm install` on root folder.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
