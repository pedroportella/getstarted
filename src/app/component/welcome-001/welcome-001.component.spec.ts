import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Welcome001Component } from './welcome-001.component';

describe('Welcome001Component', () => {
  let component: Welcome001Component;
  let fixture: ComponentFixture<Welcome001Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Welcome001Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Welcome001Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
