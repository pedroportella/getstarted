import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './page/home/home.component';
import { AboutComponent } from './page/about/about.component';
import { WorkComponent } from './page/work/work.component';
import { ServicesComponent } from './page/services/services.component';
import { BlogComponent } from './page/blog/blog.component';
import { ContactComponent } from './page/contact/contact.component';
import { SearchComponent } from './page/search/search.component';
import { NotFoundComponent } from './page/not-found/not-found.component';
import { SharedModule } from './shared';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const appRouting: ModuleWithProviders = RouterModule.forChild([
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'work', component: WorkComponent },
  { path: 'services', component: ServicesComponent },
  { path: 'blog', component: BlogComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'search', component: SearchComponent },
  { path: '**', component: NotFoundComponent }
]);

@NgModule({
  imports: [
    appRouting,
    SharedModule,
    NgbModule
  ],
  declarations: [
  ]
})
export class AppRoutingModule {}
