import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';

import {
  FooterShortComponent,
  HeaderComponent,
  SharedModule
} from './shared';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './page/not-found/not-found.component';
import { HomeComponent } from './page/home/home.component';
import { AboutComponent } from './page/about/about.component';
import { WorkComponent } from './page/work/work.component';
import { ServicesComponent } from './page/services/services.component';
import { BlogComponent } from './page/blog/blog.component';
import { ContactComponent } from './page/contact/contact.component';
import { SearchComponent } from './page/search/search.component';
import { Welcome001Component } from './component/welcome-001/welcome-001.component';

const rootRouting: ModuleWithProviders = RouterModule.forRoot([], { useHash: false });

@NgModule({
  declarations: [
    AppComponent,
    FooterShortComponent,
    HeaderComponent,
    NotFoundComponent,
    HomeComponent,
    AboutComponent,
    WorkComponent,
    ServicesComponent,
    BlogComponent,
    ContactComponent,
    SearchComponent,
    Welcome001Component
  ],
  imports: [
    rootRouting,
    CoreModule,
    SharedModule,
    AppRoutingModule,
    BrowserModule,
    NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
