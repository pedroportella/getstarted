import { Component } from '@angular/core';

@Component({
  selector: 'app-layout-footer-short',
  templateUrl: './footer-short.component.html'
})

export class FooterShortComponent {
  currentTime: Date = new Date();
  currentYear: number = this.currentTime.getFullYear();
}
