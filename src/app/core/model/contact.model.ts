export class Contact {
  email: string;
  mobilePhone: string;
  otherPhone: string;
}
