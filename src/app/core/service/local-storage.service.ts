import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {
  constructor (
  ) {}

  set(key: string, value: string): any {
    localStorage.setItem(key, value);
  }

  get(key: string): string {
    return localStorage.getItem(key);
  }

}
