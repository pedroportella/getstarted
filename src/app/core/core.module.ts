import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  ApiService,
  LocalStorageService,
  WindowsReferenceService,
  ScrollService,
} from './service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    ApiService,
    LocalStorageService,
    WindowsReferenceService,
    ScrollService
  ],
  declarations: []
})
export class CoreModule { }

